<?php
/**
 * SAML 2.0 remote IdP metadata for simpleSAMLphp.
 *
 * Remember to remove the IdPs you don't use from this file.
 *
 * See: https://rnd.feide.no/content/idp-remote-metadata-reference
 */

$metadata['https://idp.sfsu.edu/idp/shibboleth'] = array (
	'entityid' => 'https://idp.sfsu.edu/idp/shibboleth',
	'name' =>
		array (
			'en' => 'San Francisco State University',
		),
	'description' =>
		array (
			'en' => 'San Francisco State University',
		),
	'OrganizationName' =>
		array (
			'en' => 'San Francisco State University',
		),
	'OrganizationDisplayName' =>
		array (
			'en' => 'San Francisco State University',
		),
	'url' =>
		array (
			'en' => 'http://www.sfsu.edu/',
		),
	'OrganizationURL' =>
		array (
			'en' => 'http://www.sfsu.edu/',
		),
	'contacts' =>
		array (
			0 =>
				array (
					'contactType' => 'technical',
					'givenName' => 'Supakit Kiatrungrit',
					'emailAddress' =>
						array (
							0 => 'supakitk@sfsu.edu',
						),
				),
			1 =>
				array (
					'contactType' => 'administrative',
					'givenName' => 'Joellen Fung',
					'emailAddress' =>
						array (
							0 => 'jfung@sfsu.edu',
						),
				),
			2 =>
				array (
					'contactType' => 'technical',
					'givenName' => 'Technical support',
					'emailAddress' =>
						array (
							0 => 'systems@sfsu.edu',
						),
				),
			3 =>
				array (
					'contactType' => 'administrative',
					'givenName' => 'Supakit Kiatrungrit',
					'emailAddress' =>
						array (
							0 => 'supakitk@sfsu.edu',
						),
				),
		),
	'metadata-set' => 'saml20-idp-remote',
	'SingleSignOnService' =>
		array (
			0 =>
				array (
					'Binding' => 'urn:mace:shibboleth:1.0:profiles:AuthnRequest',
					'Location' => 'https://idp.sfsu.edu/idp/profile/Shibboleth/SSO',
				),
			1 =>
				array (
					'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
					'Location' => 'https://idp.sfsu.edu/idp/profile/SAML2/Redirect/SSO',
				),
			2 =>
				array (
					'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
					'Location' => 'https://idp.sfsu.edu/idp/profile/SAML2/POST/SSO',
				),
			3 =>
				array (
					'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST-SimpleSign',
					'Location' => 'https://idp.sfsu.edu/idp/profile/SAML2/POST-SimpleSign/SSO',
				),
		),
	'SingleLogoutService' =>
		array (
		),
	'ArtifactResolutionService' =>
		array (
			0 =>
				array (
					'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
					'Location' => 'https://idp.sfsu.edu:8443/idp/profile/SAML2/SOAP/ArtifactResolution',
					'index' => 1,
				),
			1 =>
				array (
					'Binding' => 'urn:oasis:names:tc:SAML:1.0:bindings:SOAP-binding',
					'Location' => 'https://idp.sfsu.edu:8443/idp/profile/SAML1/SOAP/ArtifactResolution',
					'index' => 2,
				),
		),
	'keys' =>
		array (
			0 =>
				array (
					'encryption' => false,
					'signing' => true,
					'type' => 'X509Certificate',
					'X509Certificate' => '
MIIDGzCCAgOgAwIBAgIUaVXwsxeZ7HtpgzDRqkb/SXmxNCUwDQYJKoZIhvcNAQEF
BQAwFzEVMBMGA1UEAxMMaWRwLnNmc3UuZWR1MB4XDTEwMDExMjE3NTg0OVoXDTMw
MDExMjE3NTg0OVowFzEVMBMGA1UEAxMMaWRwLnNmc3UuZWR1MIIBIjANBgkqhkiG
9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnM2TW44FPEMlaZShdBub38iLLGtpvvtTZVvC
tLo6xRQx0YCPYIQua2hxjf5lYFMe2KlwehxuCfujsTbegBX8TNg86NaN2s5iTFk7
Q7BKY0o1UGEHhmELKZx/BVSH1AB0qwk1ZF9jGyynTITRwRGsr1yLvFM3aTH1tB4s
iMpTTp7U4HfWcfCsAepxVJw8o8Nvgujgi6E8Mh4emTz+PdNmx0cgpiu7EpvruUEk
ELlHifyFRhCCBUeOtxgsJXvXgqIYxLlxcz7aGnsylTCgX8fN2WABkJte90Cv06EU
mkCnB4x8pKLxSfVkLDRDImpqPwnTjmizD6neSRQ5ano4qdAm9wIDAQABo18wXTA8
BgNVHREENTAzggxpZHAuc2ZzdS5lZHWGI2h0dHBzOi8vaWRwLnNmc3UuZWR1L2lk
cC9zaGliYm9sZXRoMB0GA1UdDgQWBBT7bbal9a/eJjoYsQd6Stdkk9D6uTANBgkq
hkiG9w0BAQUFAAOCAQEAB2UmDPQkEVR8YPNCUZS2+XjA5Pw7vdswtZpqLbN4x2RG
AFnaUFs4Qw5/9RScVOdfuSKjbB32LSJzOTLUc2LA2zUhAcieprD9wBcW2gIVnpPC
PQxMSI7vD7YuF6erW/qFh+gZwBod/PPmP30wy4uDAcWfW09f79wWv67v8ePXS/PA
Ww8M8pNXffLcYo8NAdTY1ZddhYsqcAduFzL4Q0u/+QpV4K2qpMOK57sJ/4GuI0i4
TF+IPVqP9ibO/PX7scYZDBm8wGCQ3G3UwADAQxJlSTJzyP7PthgH3ublYb3IbrY6
OQKTDUHTSx//VpESlGrhleDG/BalOfbtxZka2UF05A==
          ',
				),
		),
	'scope' =>
		array (
			0 => 'sfsu.edu',
		),
);