<?php

// Delete all local cookies
$cookiesSet = array_keys($_COOKIE);

$domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? ("." . $_SERVER['HTTP_HOST']) : false;
for ($x=0;$x<count($cookiesSet);$x++){
   setcookie($cookiesSet[$x],"",time()-3600,"/", $domain, 0, true);
   setcookie($cookiesSet[$x],"",time()-3600,"/", "", 0, true);
}//for

// parsing IDP hostname
//$str_position =  stripos($_SERVER["Shib-Identity-Provider"], "/idp/");
//$idpHostname =  substr($_SERVER["Shib-Identity-Provider"], 0, $str_position); 

// Determine return protocol based on previous protocal type
$returnProtocol = "http";
if ($_SERVER["SERVER_PORT"] == "443"){
  $returnProtocol = "https";
}//i

// kill session and redirect to idp logout url
Header("Location:https://idp.sfsu.edu/idp/Logout");

?>
